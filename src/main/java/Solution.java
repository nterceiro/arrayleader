import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

    public int[] solution(int k, int m, int[] a) {
        int[] firstArray = modifyFirstSegment(k, a);
        int[] lastArray = modifyLastSegment(k, a);

        int[] result = new int[2];
        result[0] = findArrayLeader(m, firstArray);
        result[1] = findArrayLeader(m, lastArray);

        return result;
    }

    private int[] modifyFirstSegment(int k, int[] a) {
        int[] modified = Arrays.copyOf(a, a.length);
        for (int i = 0 ; i < k; i++) {
            modified[i] = modified[i] + 1;
        }
        return modified;
    }

    private int[] modifyLastSegment(int k, int[] a) {
        int[] modified = Arrays.copyOf(a, a.length);
        int init = modified.length - k;
        for (int i = init ; i < modified.length; i++) {
            modified[i] = modified[i] + 1;
        }
        return modified;
    }

    private int findArrayLeader(int m, int[] a) {
        List<Integer> sorted = Arrays.stream(a)
                .boxed()
                .sorted()
                .collect(Collectors.toList());

        int arrayLeader = 1;
        long maxResults = 0;
        for(int i = 1 ; i <= m + 1 ; i++) {
            int finalI = i;
            long total = sorted.stream().filter(j -> j == finalI).count();
            if (total > maxResults) {
                arrayLeader = finalI;
                maxResults = total;
            }
        }
        return arrayLeader;
    }

}
