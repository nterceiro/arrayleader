import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SolutionTest {

    private final Solution solution = new Solution();

    @Test
    public void testOne() {
        int k = 3;
        int m = 5;
        int[] a = {2,1,3,1,2,2,3};

        int[] expectedResult = {2,3};
        assertArrayEquals(expectedResult, solution.solution(k, m, a));
    }

    @Test
    public void testTwo() {
        int k = 4;
        int m = 2;
        int[] a = {1,2,2,1,2};

        int[] expectedResult = {2,3};
        assertArrayEquals(expectedResult, solution.solution(k, m, a));
    }

}
